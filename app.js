const express = require('express'),
  app = express(),
  jwt = require('jsonwebtoken') // , users = require('./users')

const host = '127.0.0.1' // localhost
const port = 7000 // for queries from the clients
  
const tokenKey = 'ba21-dc43-fe65-hg87' // some secret salt

var users = [
    {
        'id': 1,
        'login': 'user1',
        'password': 'password1',
        'email': 'advev@mail.ru',
        'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTI0MDQyLCJpYXQiOjE2NzUyNDIwNTB9.fg5HutZ8T3X-uNDQEmsQzFVUOykLau6M8vjABWtUWHk'
    }//,
    // {
    //     "id": 2,
    //     "login": "dmalex",
    //     "password": "password1",
    //     "email": "advev@mail.ru",
    //     "token": ""
    // }
]

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
  
app.use(express.json())
//
// Функция промежуточной обработки, монтируемая в путь /api/auth
// Эта функция выполняется для всех типов запросов HTTP в пути /api/auth
//
app.use('/api/auth/', (req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization.split(' ')[1],
      tokenKey,
      (err, payload) => {
        if (err) next()
        else if (payload) {
          for (let user of users) {
            if (user.id === payload.id) {
              const theUser = {
                'id': user.id,
                'login': user.login,
                'password': user.password,
                'email': user.email,
                'token': user.token
              }
              req.user = theUser // user
              next()
            }
          }
          if (!req.user) next()
        }
      }
    )
  }
  next()
})


app.post('/api/auth/checkout', (req, res) => { // Авторизация по сохраненному токену
  // console.log('POST: /api/auth/checkout', req.headers.authorization)
  // Проверка наличия токена авторизации в хедере 
  if (req.headers.authorization) { // если есть, то выделить его в "Bearer eyJhbGciOiJI1NiIs..."
    const token = req.headers.authorization.split(' ')[1]
    // и проверить его
    jwt.verify(token, tokenKey,
      (err, payload) => {
        if (err)
          return res.status(404).json({ message: 'Token is not valid' }) // устарел
        else
          // return res.status(200).json({ token }) // валидный
          return res.status(200).json({id: payload.id, token})
      }
    )    
  } else return res.status(404).json({ message: 'No token' })
})


app.post('/api/auth/signin', (req, res) => { // Авторизация пользователя
  for (let user of users) {
    if (
      req.body.login === user.login &&
      req.body.password === user.password
    ) {
      return res.status(200).json({
        id: user.id,
        login: user.login,
        token: jwt.sign({ id: user.id }, tokenKey),
      })
    }
  }
  return res.status(404).json({ message: 'User with such login and password is not found' })
})


app.post('/api/auth/signup', (req, res) => { // Регистрация пользователя
    const bodyUser = req.body
    console.log(req.body)
    // console.log(bodyUser)
    // console.log(users)
    if (JSON.stringify(bodyUser) === '{}')
        return res.status(404).json({ message: 'Empty user data' })

    if (typeof bodyUser.login === 'undefined' ||
        typeof bodyUser.password === 'undefined' ||
        typeof bodyUser.email === 'undefined') {
        return res.status(404).json({ message: 'Incorrect user data' })
    }

    if (bodyUser.login === '' ||
        bodyUser.password === '' ||
        bodyUser.email === '') {
        return res.status(404).json({ message: 'Some user data is empty' })
    }
    // console.log(users)

    for (let user of users) {
        // console.log(bodyUser.login, " : ", user.login, bodyUser.login === user.login)
        if (bodyUser.login === user.login) {
            return res.status(404).json({ message: 'Such user already exists' })
        }
    }
    const newId = getRandomInt(999999)
    let newUser = {
        id: newId,
        login: bodyUser.login,
        password: bodyUser.password,
        email: bodyUser.email,
        token: jwt.sign({ id: newId }, tokenKey)
    }
    users.push(newUser)
    // return res.status(200).json(newUser)
    return res.status(200).json({
        id: newUser.id,
        login: newUser.login,
        password: newUser.password, // test only
        email: newUser.email,
        token: newUser.token
    })
})
  

app.get('/api/auth/user', (req, res) => {
    // console.log(req)
    if (req.user) return res.status(200).json(req.user)
    else return res.status(401).json({ message: 'Not authorized' })
})


app.get('/api/auth/data', (req, res) => {
    // console.log(req)
    if (req.user) return res.status(200).json({ data: 'some data' })
    else return res.status(401).json({ message: 'Not authorized' })
})


app.listen(port, host, () =>
  console.log(`Server listens http://${host}:${port}`)
)
